export class BaseInput<T>{
  key: string;
  value: T;
  label: string;
  order: number;
  required: boolean;
  constructor(options: {
    key?: string,
    value?: T,
    label?: string,
    order?: number,
    required?: boolean
  } = {}) {
    this.value = options.value;
    this.key = options.key;
    this.label = options.label;
    this.required = options.required;
    this.order = options.order;
  }
}