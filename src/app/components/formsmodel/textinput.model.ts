import { BaseInput } from './baseinput.model'

export class TextInput extends BaseInput<string>{
  constructor(options = {}) {
    super(options);
  }
}