import { Component } from '@angular/core';
import { DynamicFormService } from '../../services/forms.service';
import { FormFieldProvider } from '../../services/formfield-provider.services'
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
  selector: 'dynamic-forms',
  templateUrl: './dynamic-forms.component.html',
  styleUrls: ['./dynamic-forms.component.css']
})

export class DynamicForm {
  formGroups: FormGroup;
  FormFields = null;
  canRender = false;
  arr = [
    { name: 'username' },
    { name: 'mail' },

  ]
  constructor(
    private dynamicFormService: DynamicFormService,
    private formFieldProvider: FormFieldProvider) {



  }
  ngOnInit() {
    this.FormFields = this.formFieldProvider.getFieilds();
    this.formGroups = this.dynamicFormService.toFormGroup(this.FormFields);
    this.canRender = true;
    console.log(this.FormFields, this.formGroups)
  }
  print(field) {

  }
}  