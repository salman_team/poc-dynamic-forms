import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms'

@Component({
  selector: 'centerpanel',
  templateUrl: './centerpanel.component.html',
  styleUrls: ['./centerpanel.component.css']
})

export class CenterPanel {
  userForm;
  Fields = [
    {
      name: 'name',
      type: 'text',
      value: 'salman'
    },
    {
      name: 'age',
      type: 'text',
      value: 24
    },
    {
      name: 'gender',
      type: 'radio',
      value: []
    },

  ]

  constructor(private fb: FormBuilder) {
    this.loadForm(this.Fields)
  }

  loadForm(Fields) {
    let localField = {};
    Fields.forEach(field => {
      localField[field.name] = [];
    });
    this.userForm = this.fb.group({
      name: ['salman'],
      gender:['male']
    })
  }

  onUserSave(values) {
    console.log(values)
  }

}