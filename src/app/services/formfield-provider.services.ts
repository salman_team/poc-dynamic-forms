import { Injectable } from '@angular/core'

import { TextInput } from '../components/formsmodel/textinput.model';

@Injectable()

export class FormFieldProvider {

  FieldList = [
    new TextInput({
      key: 'name',
      value: 'salman',
      label: 'User First Name',
      required: true
    }),
    new TextInput({
      key: 'mobile',
      value: '8056947504',
      label: 'User Mobile Number',
      required: true
    })
  ]
  constructor() {

  }

  getFieilds() {
    return this.FieldList;
  }

}