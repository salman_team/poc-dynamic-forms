import { FormBuilder, FormControl } from '@angular/forms';
import { Injectable } from '@angular/core'
@Injectable()

export class DynamicFormService {
  constructor(private fb: FormBuilder) {

  }

  toFormGroup(fields) {
    let formsArray = [];
    fields.forEach(element => {
      formsArray[element.key] = new FormControl(element.value);
    });
    return this.fb.group(formsArray);
  }
}