import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

// import { Home } from './components/layoutes/home/home.component';
import { DynamicForm } from './components/dynamic-forms/dynamic-forms.component'
import { Public } from './layouts/public/public.component'
import { Header } from './components/header/header.component';
import { LeftPanel } from './components/leaftpanel/leftpanel.component';
import { CenterPanel } from './components/centerpanel/centerpanel.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/staticform' },
  { path: 'dynamicform', component: DynamicForm },
  { path: 'staticform', component: Public }
]


@NgModule({
  declarations: [
    DynamicForm,
    Public,
    Header,
    LeftPanel,
    CenterPanel
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [

  ],
  exports: [
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    Public,
    DynamicForm,
    Header
  ]
})

export class MyRouter {
  constructor() {

  }
}