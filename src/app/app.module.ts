import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
// custom modules 
import { MyRouter } from './router.module'

import { AppComponent } from './app.component';
import { Header } from './components/header/header.component';
import { LeftPanel } from './components/leaftpanel/leftpanel.component';
import { CenterPanel } from './components/centerpanel/centerpanel.component';


import { FormFieldProvider } from './services/formfield-provider.services';
import { DynamicFormService } from './services/forms.service'


@NgModule({
  declarations: [
    AppComponent,

  ],
  exports: [
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MyRouter
  ],
  providers: [
    FormFieldProvider,
    DynamicFormService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
